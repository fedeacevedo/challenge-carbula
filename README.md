# Challenge Cárbula

Este proyecto se utilizará para resolver un desafío de programación.

# Objetivos

Conocer el manejo de las tecnologías base de los participantes y observar su forma de trabajo antes dudas y/o obstáculos presentados.

### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Java
Maven
MySql
Lombok
```
### Instalación 🔧

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

```
Instalar Java 8
```
```
Instalar Maven
```
```
Instalar MySQL, crear e importar la bd de ejemplo. (Se adjunta)
```
```
Instalar la librería Lombok en la raiz del Ide utilizado (https://projectlombok.org/)
```
```
Descargar Repositorio (https://gitlab.com/fedeacevedo/challenge-carbula.git)
```
```
Crear una rama con el siguiente formato: 'challenge_' + tu nombre + '_' + 'tu apellido' desde la rama 'develop' - Ej: challenge_federico_acevedo
```
```
Levantar el proyecto con el Ide que deseen
```
```
Realizar los cambios convenientes y hacer push a la rama creada.
```

## Construido con 🛠️

* [Spring](https://spring.io/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias


